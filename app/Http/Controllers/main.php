<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use App\users;
use App\image;
use Illuminate\Support\Facades\DB;

class main extends Controller
{
    public function read(){
        return student::all();
    }
    public function update(Request $req){
        $student = student::find($req->id);
        $student->date_of_admission = $req->date_of_admission;
        $student->date_of_test = $req->date_of_test;
        $student->status = $req->status;
        $student->reason_reject = $req->reason_reject;
        $student->admission_signature = $req->admission_signature;
        $student->principal_signature = $req->principal_signature;
        $student->finance_signature = $req->finance_signature;
        $student->save();

        if($student->save()){
            $response = [
                'status' => 'true',
                'message' => "Storing Data Success",
                'data' => $student
            ];
        } else {
            $response = [
                'status' => FALSE,
                'message' =>  "Storing Data Fail",
                'data' => '-'
            ];
        }
        return $response;
    }
    public function create(Request $req){
        $student = new student;
        $student->full_name = $req->full_name;
        $student->sex = $req->sex;
        $student->grade = $req->grade;
        $student->place_of_birth = $req->place_of_birth;
        $student->date_of_birth = $req->date_of_birth;
        $student->address = $req->address;
        $student->phone = $req->phone;
        $student->nationality = $req->nationality;
        $student->religion = $req->religion;
        $student->living = $req->living;
        $student->name_of_school = $req->name_of_school;
        $student->address_of_school = $req->address_of_school;
        $student->grade_of_school = $req->grade_of_school;
        $student->father_name = $req->father_name;
        $student->father_place_of_birth = $req->father_place_of_birth;
        $student->father_date_of_birth = $req->father_date_of_birth;
        $student->father_address = $req->father_address;
        $student->father_mobile_phone = $req->father_mobile_phone;
        $student->father_email = $req->father_email;
        $student->father_nationality = $req->father_nationality;
        $student->father_occupation = $req->father_occupation;
        $student->father_company = $req->father_company;
        $student->mother_name = $req->mother_name;
        $student->mother_place_of_birth = $req->mother_place_of_birth;
        $student->mother_date_of_birth = $req->mother_date_of_birth;
        $student->mother_address = $req->mother_address;
        $student->mother_mobile_phone = $req->mother_mobile_phone;
        $student->mother_email = $req->mother_email;
        $student->mother_nationality = $req->mother_nationality;
        $student->mother_occupation = $req->mother_occupation;
        $student->mother_company = $req->mother_company;
        $student->sibling_1_name = $req->sibling_1_name;
        $student->sibling_1_date_of_birth = $req->sibling_1_date_of_birth;
        $student->sibling_1_sex = $req->sibling_1_sex;
        $student->sibling_1_school = $req->sibling_1_school;
        $student->sibling_1_grade = $req->sibling_1_grade;
        $student->sibling_2_name = $req->sibling_2_name;
        $student->sibling_2_date_of_birth = $req->sibling_2_date_of_birth;
        $student->sibling_2_sex = $req->sibling_2_sex;
        $student->sibling_2_school = $req->sibling_2_school;
        $student->sibling_2_grade = $req->sibling_2_grade;
        $student->sibling_3_name = $req->sibling_3_name;
        $student->sibling_3_date_of_birth = $req->sibling_3_date_of_birth;
        $student->sibling_3_sex = $req->sibling_3_sex;
        $student->sibling_3_school = $req->sibling_3_school;
        $student->sibling_3_grade = $req->sibling_3_grade;
        $student->guardian_name = $req->guardian_name;
        $student->guardian_address = $req->guardian_address;
        $student->guardian_phone = $req->guardian_phone;
        $student->guardian_relationship = $req->guardian_relationship;
        $student->guardian_occupation = $req->guardian_occupation;
        $student->father_signature = $req->father_signature;
        $student->mother_signature = $req->mother_signature;
        $student->date_of_admission = $req->date_of_admission;
        $student->date_of_test = $req->date_of_test;
        $student->status = $req->status;
        $student->reason_reject = $req->reason_reject;
        $student->admission_signature = $req->admission_signature;
        $student->principal_signature = $req->principal_signature;
        $student->finance_signature = $req->finance_signature;
        $student->save();

        if($student->save()){
            $response = [
                'status' => 'true',
                'message' => "Storing Data Success",
                'data' => $student
            ];
        } else {
            $response = [
                'status' => FALSE,
                'message' =>  "Storing Data Fail",
                'data' => '-'
            ];
        }
        return $response;
    }
    public function find($id){
        $data = student::find($id);
        return $data;
    }
    public function find_agree($id){
        $users = DB::table('student')
            ->leftJoin('admission', 'admission.id_student', '=', 'admission.id_student')
            ->where('admission.id_student','=',$id)
            ->get();
        return $users;

    }

    public function login(Request $req){
        $username = $req->username;
        $password = md5($req->password);

        $validation = users::where('username',$username)
            ->where('password', $password)
            ->count();
        if ($validation == 1){
            $data = users::where('username',$username)
                ->where('password',$password)
                ->first();
            return response()->json(
                [
                    'status'=>'true',
                    'message'=>'success',
                    'data'=>$data
                ]
            );
        }else{
            return response()->json(
                [
                    'status'=>'false',
                    'message'=>'error',
                    'data'=>'password atau username salah'
                ]
            );
        }
    }
    public function image($name){
        $image = image::where('type',$name)->get();
        return $image;
    }
}
