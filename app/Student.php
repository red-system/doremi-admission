<?php


namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = 'student';
    protected $fillable = [
        'full_name',
        'grade',
        'sex',
        'place_of_birth',
        'date_of_birth',
        'address',
        'phone',
        'nationality',
        'religion',
        'living',
        'name_of_school',
        'address_of_school',
        'grade_of_school',
        'father_name',
        'father_place_of_birth',
        'father_date_of_birth',
        'father_address',
        'father_mobile_phone',
        'father_email',
        'father_nationality',
        'father_occupation',
        'father_company',
        'mother_name',
        'mother_place_of_birth',
        'mother_date_of_birth',
        'mother_address',
        'mother_mobile_phone',
        'mother_email',
        'mother_nationality',
        'mother_occupation',
        'mother_company',
        'sibling_1_name',
        'sibling_1_date_of_birth',
        'sibling_1_sex',
        'sibling_1_school',
        'sibling_1_grade',
        'sibling_2_name',
        'sibling_2_date_of_birth',
        'sibling_2_sex',
        'sibling_2_school',
        'sibling_2_grade',
        'sibling_3_name',
        'sibling_3_date_of_birth',
        'sibling_3_sex',
        'sibling_3_school',
        'sibling_3_grade',
        'guardian_name',
        'guardian_address',
        'guardian_phone',
        'guardian_relationship',
        'guardian_occupation',
        'father_signature',
        'mother_signature',
        'date_of_admission',
        'date_of_test',
        'statue',
        'reason_reject',
        'admission_signature',
        'principal_signature',
        'finance_signature'
    ];
    public $timestamps =  false; //untyk menghentikan pemberian waktu otomatis
    protected $primaryKey = 'id_student'; // Misal kita memakai nama id_kendaraan

}
