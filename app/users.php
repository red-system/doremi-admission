<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class users extends Model
{
    protected $table = 'user';
    public $timestamps =  false; //untyk menghentikan pemberian waktu otomatis
    protected $primaryKey = 'id'; // Misal kita memakai nama id_kendaraan
}
