<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class image extends Model
{
    protected $table = 'image';
    public $timestamps =  false; //untyk menghentikan pemberian waktu otomatis
    protected $primaryKey = 'id'; // Misal kita memakai nama id_kendaraan
}
