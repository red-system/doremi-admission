<?php


Route::get('/student/read','main@read');
Route::get('/student/find/{id}','main@find');
Route::get('/student/image/{name}','main@image');
Route::put('/student/agree','main@update');
Route::get('/student/findagree/{id}','main@find_agree');
Route::post('/student/create','main@create');
Route::post('/login','main@login');
